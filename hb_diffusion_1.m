function res=hb_diffusion(A,opts)

if (nargin<=1)
    opts.affichage=true;
    opts.tauLength=20;
    opts.tauStep=1;
    opts.frameRate=60;
    opts.tailleFilm=120;
    opts.scale=1;
    opts.temperature=298;
    opts.partDiameter=2e-6;
    opts.curvefittingtoolbox=false;
end

N=floor(opts.tauLength/opts.tauStep);
variance=zeros(N,1);
varianceY=zeros(N,1);
varianceX=zeros(N,1);

Ncorr=zeros(N,1);
Vmoy=zeros(N,2);


%collecte des correlations et rescaling
h=waitbar(0,'Correlations in progress...');
opts.tailleFilm=max(A(:,3));
for (i=1:N)
    
	r=hb_diffusion_tau(A,i*opts.tauStep,opts.tailleFilm,1,opts.tailleFilm); 
	variance(i)=r.variance*opts.scale^2;
    varianceY(i)=r.varianceY*opts.scale^2;
    varianceX(i)=r.varianceX*opts.scale^2;
    Ncorr(i)=r.Ncorr;
    Vmoy(i,:)=r.Vmoyenne'*opts.scale*opts.frameRate/opts.tauStep;
    waitbar(sqrt(i/N),h);
end


close(h);

temps=[opts.tauStep:opts.tauStep:opts.tauLength]'/opts.frameRate;
%assignin('base', 'Ncorr', Ncorr);

if (length(Ncorr(Ncorr>0))>3)
    if exist('fit')
    %fit lin%
        fopt = fitoptions('poly1','Weights',2*sqrt(Ncorr(Ncorr>0))./variance(Ncorr>0),'Lower',[0 0]);
        cfun1= fit(temps(Ncorr>0),variance(Ncorr>0)*1e12,'poly1',fopt);
        confidence_int1 = confint(cfun1);  
        diffusion_coef1=cfun1.p1/4*1e-12;
        
    %fit 2nd% 
        fopt = fitoptions('poly2','Weights',2*sqrt(Ncorr(Ncorr>0))./variance(Ncorr>0),'Lower',[0 0 0]);
        cfun = fit(temps(Ncorr>0),variance(Ncorr>0)*1e12,'poly2',fopt);
        confidence_int = confint(cfun);  
        diffusion_coef=cfun.p2/4*1e-12;
    else
        cfun1= polyfit(temps(Ncorr>0),variance(Ncorr>0)*1e12,1);
        confidence_int1=NaN;       
        diffusion_coef1=cfun1(1)/4*1e-12;
            
        cfun= polyfit(temps(Ncorr>0),variance(Ncorr>0)*1e12,2);
        confidence_int=NaN;       
        diffusion_coef=cfun(2)/4*1e-12;
    end
       fitOk=true;
else
        diffusion_coef=NaN;
        diffusion_coef1=NaN;
        fitOk=false;
end


res.Vmoy=Vmoy;
res.variance=variance;
res.varianceY=varianceY;    
res.varianceX=varianceY;
res.temps=temps;
res.Ncorr=Ncorr;
res.diff_coef=diffusion_coef;
res.diff_coef1=diffusion_coef1;
res.fitres=cfun;
res.fitres1=cfun1;
res.confidence=confidence_int;
res.confidence1=confidence_int1;

res.viscosity=1.3806503e-23*opts.temperature/(3*pi*res.diff_coef*opts.partDiameter);

res.viscosityConfidence=1.3806503e-23*opts.temperature./(3*pi*res.confidence(:,1)*opts.partDiameter);

if ~exist('fit')
    res.viscosityConfidence=[NaN,NaN];
end

if (opts.affichage)
    figure();
    errorbar(temps,variance, sqrt(2)*variance(:,1)./sqrt(Ncorr(:,1)),'ko' );
    hold on;
    if (fitOk>0)
      if exist('fit')
          plot(temps,feval(cfun1,temps)*1e-12,'r');
        plot(temps,feval(cfun,temps)*1e-12,'b');
      else
            plot(temps,polyval(cfun1,temps)*1e-12,'r');
        plot(temps,polyval(cfun,temps)*1e-12,'b')
      end
    end

    xlim([0 max(temps)]);
    ylim([0 max(variance)*1.1]);
    xlabel('Time (s)');
    ylabel('Mean square displacements');
    legend('Exp. points','1st order fit','2nd order fit','location','NorthWest');
    t1=text(max(temps)/3,max(variance)/5,['Diffusion coefficient : ' num2str(res.diff_coef)]);
    t2=text(max(temps)/3,max(variance)/8,['Viscosity : ' sprintf('%0.3e', res.viscosity) 'Pa.s ' '(+/- ' num2str((-res.viscosityConfidence(2)+res.viscosityConfidence(1)),2) ' )']);
    prettyFig();
    set(t1,'FontSize',12);
    set(t2,'FontSize',12);

    hold off;

end

end
