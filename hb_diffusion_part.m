function res=hb_diffusion_part(A,opts)

if (nargin<=1)
    opts.affichage=true;
    opts.tauLength=7;
    opts.tauStep=1;
    opts.frameRate=60;
    opts.tailleFilm=120;
    opts.scale=1;
    opts.fit=1; % fit = 1 : powerlaw fit; fit=2 linear fit;
    opts.dispRaw=true;
    opts.dispStats=true;
    opts.dispMap=false;
end
correctMeanDisp=true;

N=floor(opts.tauLength/opts.tauStep);

%collecte des correlations et rescaling
h=waitbar(0,'Correlations in progress...');

Np=max(A(:,4));

tau=opts.tauStep:opts.tauStep:opts.tauLength;
    tauinterp=logspace(log10(opts.tauStep),log10(opts.tauLength),100);

if  opts.dispRaw 
    figure(52)
    clf
end

%Np=5;

 res.part=struct('tau',[],'Deltar2',[],'D',[],'alpha',[],'beta',[],'x',[],'y',[]);
 
%compute mean disp
if correctMeanDisp
    Nt=max(A(:,3));
    uimoy=zeros(Nt-1,2)*NaN;
    for t=1:Nt-1
        Pi1=A(A(:,3)==t,:);
        Pi2=A(A(:,3)==t+1,:);
        ui=[];
        for k=1:length(Pi1(:,4))
            k0=find(Pi2(:,4)==Pi1(k,4),1);
            if (k0>0)
                ui=[ui ; (Pi2(k0,1:2)-Pi1(k,1:2))];			
            end
        end
        if (size(ui,1)>1)			
                uim=mean(ui);
                uimoy(t,:)=uim;
        end

    meanpos=[[0 0]; cumsum(uimoy,1)];
    end
figure(25)
plot(meanpos)
    meanpos(:,1)
    meanpos(:,2)

end


figure(52)

for i=1:Np  % loop on all the particles in order to compute Delta r^2

    traj=A(A(:,4)==i,1:3);
    
    Deltar2=zeros(size(tau))*NaN;
    NN=sum(tau<length(traj)/5); % do not unerstand
    for j=1:NN % loop on tau values
        u2=[];
        for t=min(traj(:,3)):max(traj(:,3))-tau(j)
            
            u=traj(traj(:,3)==t+tau(j),1:2)-traj(traj(:,3)==t,1:2);
            if (correctMeanDisp)
                u=u-(meanpos(t+tau(j),1:2)-meanpos(t,1:2));
            end
            if ~isempty(u)
                u2=[u2 u(1)^2+u(2)^2];
            end
        end
        Deltar2(j)=mean(u2)*opts.scale^2;        
    end
   if  opts.dispRaw 
       plot(tau/opts.frameRate,Deltar2,'-','displayname',sprintf('Np=%g, length %g',i,length(traj)));
       disp(i)
       disp(Deltar2(1)) 
        hold on
        if i==1
              set(gca, 'XScale','log')
              set(gca, 'YScale','log')
        end
   end
    
    if opts.fit==1
        Deltar2interp = interp1(tau,Deltar2,tauinterp);
        if length(tauinterp(~isnan(Deltar2interp)))>2
        resfit=polyfit(log(tauinterp(~isnan(Deltar2interp))/opts.frameRate),log(Deltar2interp(~isnan(Deltar2interp))),1);
        else
            resfit=[NaN NaN];
        end
        alpha(i)=resfit(1);
        
        res.part(i)=struct('tau',tau/opts.frameRate,'Deltar2',Deltar2,'D',NaN,'alpha',alpha(i),'beta',exp(resfit(2)),'x',mean(traj(:,1)),'y',mean(traj(:,2)) );
    elseif opts.fit==2
          Deltar2interp = interp1(tau,Deltar2,tauinterp);
         if length(tauinterp(~isnan(Deltar2interp)))>2
           resfit=polyfit(tauinterp(~isnan(Deltar2interp))/opts.frameRate,Deltar2interp(~isnan(Deltar2interp)),1);
         else
          
            resfit=[NaN NaN];
        end   
        alpha(i)=resfit(1)/4;        
        res.part(i)=struct('tau',tau/opts.frameRate,'Deltar2',Deltar2,'D',alpha(i),'alpha',NaN,'beta',NaN,'x',mean(traj(:,1)),'y',mean(traj(:,2)) );
    end

    waitbar(sqrt(i/Np),h);
end
if (opts.dispRaw)
    set(gca, 'XScale','log')
    set(gca, 'YScale','log')
    xlabel('\tau (s)')
    ylabel('\Delta r^2 (m^2)')
end
close(h);

if  opts.dispStats 
    figure(53)
    if opts.fit==1
        h=histogram(alpha,linspace(0,2,20));
    else
        h=histogram(alpha,20);
    end
    res.histogram=h;
end

if  opts.dispMap 
    if opts.fit==1
        F = scatteredInterpolant([res.part.x]',[res.part.y]',[res.part.alpha]');
    else
        F = scatteredInterpolant([res.part.x]',[res.part.y]',[res.part.D]');
    end
    figure(54)
    if ~isfield(opts,'sizeX')
        opts.sizeX=max(ceil(A(:,1)));
        opts.sizeY=max(ceil(A(:,2)));
    end
    [X,Y] = ndgrid(1:opts.sizeX,1:opts.sizeY);
    im=F(X,Y);
    imagesc(im)
    res.map=im;
end





end
