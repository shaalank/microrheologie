
function res=hb_diffusion_tau(A,tau,nb_image,im_start,taille_film)
% cette fonction effectue la corr�lation de position entre les images
% espac�es de tau sur un max de nb_image, � partir de l'imagfe im_start
% NOTE : tau est en unit� de temps d'image 
% taille_film contient la taille du film

%u2 contient les variances
u2=[];
u2x=[];
u2y=[];
uimoy=[];
 
for i = im_start:min(im_start+nb_image-tau-1,taille_film) %length(temps)-1;
	
	Pi1=A(A(:,3)==i,:);
	Pi2=A(A(:,3)==i+tau,:);
	
	
    % le vecteur ui enregistre les d�placements et est r�initiualis� pour
    % chaque couple d'image 
    ui=[];
    
    
	for k=1:length(Pi1(:,4))
		k0=find(Pi2(:,4)==Pi1(k,4),1);
		if (k0>0)
			ui=[ui ; (Pi2(k0,1:2)-Pi1(k,1:2))];			
		end
    end

	if (size(ui,1)>1)			
			uim=mean(ui);
            uimoy=[uimoy; uim];
			uietoile=ui-(ones(length(ui),1)*uim);
			u2=[u2 (uietoile(:,1).^2+uietoile(:,2).^2)'];
			u2y=[u2y (uietoile(:,2).^2)'];
            u2x=[u2x (uietoile(:,1).^2)'];
     end
end

% regroupage des r�sultats
res.variance=NaN;
res.varianceY=NaN;
res.varianceX=NaN;
res.Ncorr=0;
res.Vmoyenne=zeros(1,2);

if (length(uimoy)>0)
        res.variance=mean(u2);
        res.varianceY=mean(u2y);
        res.varianceX=mean(u2x);
        res.Ncorr=length(u2);
        res.Vmoyenne(:)=mean(uimoy);
end


end
		
		
