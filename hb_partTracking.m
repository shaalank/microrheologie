function tr=hb_partTracking(vid,optstr)
%ASSUME that vid is a hb_video object, and that it already does bpass and pkfnd

if nargin==1
    optstr=struct('mem',3,'good',10,'quiet',true,'dim',2,'display',true,'tr',true);
end

cnt2=[];
h=hb_waitbar();
for i=1:length(vid.time)
 vid=vid.read(i);
 cnt=cntrd(vid.currentIm,vid.postOperationsInfos.pkfnd.results,ceil((vid.postOperationsInfos.bpass(2)+2)/2)*2-1);
 cnt2=[cnt2; [cnt(:,1:2) i*ones(size(cnt,1),1)]];
 if mod(i,5)==0
     hb_waitbar(h,i/length(vid.time));
 end
end
close(h)


tr=track(cnt2,10,optstr);
disp(sprintf('Mean track length : %g', getMeanTrackLength(tr)));

if optstr.display
    figure(51)
    clf
    [p,f]=fileparts(vid.fullpath);
    plotTrack(tr);
    set(gca,'ydir','reverse','DataAspectRatioMode','manual','DataAspectRatio',[1 1 1])
    title(f)
end

end
