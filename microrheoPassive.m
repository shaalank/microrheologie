function varargout = microrheoPassive(varargin)
% MICRORHEOPASSIVE MATLAB code for microrheoPassive.fig
%      MICRORHEOPASSIVE, by itself, creates a new MICRORHEOPASSIVE or raises the existing
%      singleton*.
%
%      H = MICRORHEOPASSIVE returns the handle to a new MICRORHEOPASSIVE or the handle to
%      the existing singleton*.
%
%      MICRORHEOPASSIVE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MICRORHEOPASSIVE.M with the given input arguments.
%
%      MICRORHEOPASSIVE('Property','Value',...) creates a new MICRORHEOPASSIVE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before microrheoPassive_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to microrheoPassive_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help microrheoPassive

% Last Modified by GUIDE v2.5 27-Oct-2022 12:27:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @microrheoPassive_OpeningFcn, ...
                   'gui_OutputFcn',  @microrheoPassive_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before microrheoPassive is made visible.
function microrheoPassive_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to microrheoPassive (see VARARGIN)

% Choose default command line output for microrheoPassive
set(handles.compute,'enable','off')
set(handles.save,'enable','off')
set(handles.saveFile,'enable','off')
handles.output = hObject;
handles.video=[];
handles.partPositions=[];
handles.resultCorPerPart=[];
handles.resultCorEns=[];
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes microrheoPassive wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = microrheoPassive_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in load.
function load_Callback(hObject, eventdata, handles)
% hObject    handle to load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isa(handles.video,'hb_avi2')
h=hb_video214(handles.video);
else
h=hb_video214(hb_avi2());

hdhb_video=guidata(h);
set(hdhb_video.bpFilter,'value',1);
hdhb_video.bpFilter.Callback(h,hdhb_video);
maxx=str2double(get(hdhb_video.LimitsAffSup,'string'));
set(hdhb_video.pkfndImin,'string',num2str(round(maxx/4)));
set(hdhb_video.pkfnd,'value',1);
hdhb_video.pkfnd.Callback(h,hdhb_video);
end
set(h,'CloseRequestFcn',{@handleStateChange,hObject,handles});

% handles.hdhb_video=hdhb_video;
% handles.h=h;
% guidata(hObject,handles)



function handleStateChange(h,hand,ho,handles)
hdhb_video=guidata(h);
handles=guidata(ho);
handles.video=hdhb_video.video;
guidata(ho,handles)
updateDisplay(handles)
delete(h)
%assignin('base','hdhb_video2',hdhb_video)

function updateDisplay(handles)
if isa(handles.video,'hb_avi2')
    str={[handles.video.filename ],...
        ['FPS : '  num2str(handles.video.aviInfo.FramesPerSecond) ';  NumFrames ' num2str(handles.video.aviInfo.FramesPerSecond)] ...
        [ num2str(handles.video.aviInfo.Width) 'x' num2str(handles.video.aviInfo.Height) '; '  ... 
        'Imin = ' num2str(handles.video.postOperationsInfos.pkfnd.Imin) ]};
    set(handles.textInfoFile,'string',str)
    set(handles.compute,'enable','on');

else
    set(handles.compute,'enable','off');
    %set(handles.compute,'enable',0);
    %set(handles.compute,'enable',0);
    %set(handles.compute,'enable',0);
    
end
if ~isempty(handles.partPositions) 
    str=sprintf('%g particles detected / frame',length(handles.partPositions)/length(handles.video.time));
    set(handles.textInfoLocate,'string',str);
end
if ~isempty(handles.partPositions) && size(handles.partPositions,2)>=4
    str=sprintf('Trackind done. Mean track length : %g',getMeanTrackLength(handles.partPositions));
    set(handles.textInfoTrack,'string',str);
end
if ~isempty(handles.resultCorPerPart) 
    str=sprintf('Correlations done. ')
    set(handles.textInfoCor,'string',str);
end
if ~isempty(handles.resultCorEns) 
    str=sprintf('Correlations done. ')
    set(handles.textInfoCorEns,'string',str);
end


% --- Executes on button press in save.
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
res.video=handles.video;
res.positions=handles.partPositions;
res.resultCorPerPart=handles.resultCorPerPart;
res.resultCorEns=handles.resultCorEns;
answer = inputdlg('Enter variable name','save',1,{handles.video.filename});
assignin('base',answer{1},res)



% --- Executes on button press in compute.
function compute_Callback(hObject, eventdata, handles)
% hObject    handle to compute (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.compute,'enable','off'); drawnow

if get(handles.locateParticles,'value') && isa(handles.video,'hb_avi2') 
  set(handles.textInfoLocate,'string','LOCATING PARTICLES IN PROGESS !!!')
  drawnow
  handles.partPositions=hb_locatePart(handles.video);
  str=sprintf('%g particles detected / frame',length(handles.partPositions)/length(handles.video.time));
  set(handles.textInfoLocate,'string',str);drawnow
end

if get(handles.trackParticles,'value') && ~isempty(handles.partPositions)
    set(handles.textInfoTrack,'string','TRACKING IN PROGESS !!!')
    drawnow
    good=str2double(get(handles.minTrackLength,'string'));
    maxDisp=str2double(get(handles.maxDisp,'string'));
    optstr=struct('mem',0,'good',round(good),'quiet',true,'dim',2);
    [~,I] = sort(handles.partPositions(:,3));
    handles.partPositions=track(handles.partPositions(I,1:3),round(maxDisp),optstr);
    str=sprintf('Trackind done. Mean track length : %g',getMeanTrackLength(handles.partPositions));
    set(handles.textInfoTrack,'string',str);drawnow
end

if get(handles.showTraj,'value') && ~isempty(handles.partPositions) && size(handles.partPositions,2)>=4
    figure(51)
    clf
    partPositions=handles.partPositions;
    partPositions(:,1)=partPositions(:,1)*handles.video.aviInfo.PixelSizeX;
    partPositions(:,2)=partPositions(:,2)*handles.video.aviInfo.PixelSizeY;
    plotTrack(partPositions);
    xlabel('x (\mum)')
    ylabel('y (\mum)')
    set(gca,'ydir','reverse','DataAspectRatioMode','manual','DataAspectRatio',[1 1 1],'box','on')
    set(gcf,'color',[1 1 1])
end


if get(handles.computeCorrelations,'value') && ~isempty(handles.partPositions) && size(handles.partPositions,2)>=4
     set(handles.textInfoCor,'string','COMPUTING CORRELATIONS IN PROGESS !!!'); drawnow
     handles.video.aviInfo.PixelSizeX=1;
     opts=struct('tauStep',str2double(get(handles.tauStepPart,'String')), ...
                'tauLength',str2double(get(handles.tauMaxPart,'String')), ... 
                'fit',get(handles.fit,'Value'), ... 
                'dispRaw',get(handles.dispRaw,'Value'), ... 
                'dispStats',get(handles.dispStats,'Value'), ... 
                'dispMap',get(handles.dispMap,'Value'), ...
                'frameRate',handles.video.aviInfo.FramesPerSecond, ...
                'scale', handles.video.aviInfo.PixelSizeX*1e-6);
    handles.resultCorPerPart=hb_diffusion_part(handles.partPositions,opts);
    str=sprintf('Correlations done. ');
    set(handles.textInfoCor,'string',str); drawnow;
end
if get(handles.ensAveraging,'value') && ~isempty(handles.partPositions) && size(handles.partPositions,2)>=4
    set(handles.textInfoCorEns,'string','COMPUTING CORRELATIONS IN PROGESS !!!'); drawnow
    opts=struct('tauStep',str2double(get(handles.tauStep,'String')), ...
                'tauLength',str2double(get(handles.tauMax,'String')), ... 
                'temperature',str2double(get(handles.temperature,'String')), ... 
                'partDiameter',str2double(get(handles.particleSize,'String'))*1e-6, ... 
                'affichage',get(handles.showResults,'Value'), ... 
                'frameRate',handles.video.aviInfo.FramesPerSecond, ...
                'scale', handles.video.aviInfo.PixelSizeX*1e-6);
    handles.resultCorEns=hb_diffusion(handles.partPositions,opts);
    str=sprintf('Diff Coef : %g m^2/s; Vicosity : %g Pa.s',handles.resultCorEns.diff_coef,handles.resultCorEns.viscosity );
    set(handles.textInfoCorEns,'string',str); drawnow;
end


set(handles.compute,'enable','on')
set(handles.save,'enable','on')
set(handles.saveFile,'enable','on')
guidata(hObject,handles)





function minTrackLength_Callback(hObject, eventdata, handles)

function minTrackLength_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function showTraj_Callback(hObject, eventdata, handles)

function ensAveraging_Callback(hObject, eventdata, handles)

function computeCorrelations_Callback(hObject, eventdata, handles)

function locateParticles_Callback(hObject, eventdata, handles)

function trackParticles_Callback(hObject, eventdata, handles)

function maxDisp_Callback(hObject, eventdata, handles)

function maxDisp_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tauStepPart_Callback(hObject, eventdata, handles)
% hObject    handle to tauStepPart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tauStepPart as text
%        str2double(get(hObject,'String')) returns contents of tauStepPart as a double


% --- Executes during object creation, after setting all properties.
function tauStepPart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tauStepPart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tauMaxPart_Callback(hObject, eventdata, handles)
% hObject    handle to tauMaxPart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tauMaxPart as text
%        str2double(get(hObject,'String')) returns contents of tauMaxPart as a double


% --- Executes during object creation, after setting all properties.
function tauMaxPart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tauMaxPart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in fit.
function fit_Callback(hObject, eventdata, handles)
% hObject    handle to fit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns fit contents as cell array
%        contents{get(hObject,'Value')} returns selected item from fit


% --- Executes during object creation, after setting all properties.
function fit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function temperature_Callback(hObject, eventdata, handles)
% hObject    handle to temperature (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of temperature as text
%        str2double(get(hObject,'String')) returns contents of temperature as a double


% --- Executes during object creation, after setting all properties.
function temperature_CreateFcn(hObject, eventdata, handles)
% hObject    handle to temperature (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function particleSize_Callback(hObject, eventdata, handles)
% hObject    handle to particleSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of particleSize as text
%        str2double(get(hObject,'String')) returns contents of particleSize as a double


% --- Executes during object creation, after setting all properties.
function particleSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to particleSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tauStep_Callback(hObject, eventdata, handles)
% hObject    handle to tauStep (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tauStep as text
%        str2double(get(hObject,'String')) returns contents of tauStep as a double


% --- Executes during object creation, after setting all properties.
function tauStep_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tauStep (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tauMax_Callback(hObject, eventdata, handles)
% hObject    handle to tauMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tauMax as text
%        str2double(get(hObject,'String')) returns contents of tauMax as a double


% --- Executes during object creation, after setting all properties.
function tauMax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tauMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in showResults.
function showResults_Callback(hObject, eventdata, handles)
% hObject    handle to showResults (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showResults


% --- Executes on button press in dispRaw.
function dispRaw_Callback(hObject, eventdata, handles)
% hObject    handle to dispRaw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of dispRaw


% --- Executes on button press in dispStats.
function dispStats_Callback(hObject, eventdata, handles)
% hObject    handle to dispStats (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of dispStats


% --- Executes on button press in dispMap.
function dispMap_Callback(hObject, eventdata, handles)
% hObject    handle to dispMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of dispMap


% --- Executes on button press in saveFile.
function saveFile_Callback(hObject, eventdata, handles)
% hObject    handle to saveFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[f,p]=uiputfile([handles.video.filename '_res.mat']);
video=handles.video;
positions=handles.partPositions;
resultCorPerPart=handles.resultCorPerPart;
resultCorEns=handles.resultCorEns;
save(fullfile(p,f),'video','positions','resultCorPerPart','resultCorEns');


% --- Executes on button press in force.
function force_Callback(hObject, eventdata, handles)
% hObject    handle to force (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.compute,'enable','on')
