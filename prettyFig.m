function prettyFig(siz,markerSize,fontSize,keepColors,bold,font)
%prettyFig(siz,markerSize,fontSize,keepColors)

if (nargin<1)
    siz=[400 340];
end
if (nargin<2)
    markerSize=6;
end
if (nargin<3)
    fontSize=[14 14];
end
if (nargin<4)
    keepColors=1;
end
if nargin<5
    bold=true; 
end
if nargin<6
    font='Helvetica';
end



if (length(fontSize)==1)
    fontSize=[fontSize fontSize];
end


set(gcf,'Color',[1 1 1],'units','pixel');
pos=get(gcf,'Position');
set(gcf,'WindowStyle','normal');

set(gcf,'Position',[pos(1) pos(2)  siz]);


child_handles = allchild(gcf);
for (i=1:length(child_handles))
%          get(child_handles(i),'type')
    
    if strcmp((get(child_handles(i),'type')),'axes')
    
        set(child_handles(i),'FontSize',fontSize(1),'linewidth',1,'fontname',font);
    if bold
        set(child_handles(i),'fontweight','bold')
    end
        
    axes_handles = allchild(child_handles(i));
    for (k=1:length(axes_handles))
        %disp(get(axes_handles(k),'type'))
       
        if strcmp(get(axes_handles(k),'type'),'line')==1
             set(axes_handles(k),'MarkerSize',markerSize);
             if (keepColors==0)
                 set(axes_handles(k),'MarkerFaceColor',[1 1 1]);
             end
             if (get(axes_handles(k),'LineWidth')<1)
                 set(axes_handles(k),'LineWidth',1);
             end
        end
        if strcmp(get(axes_handles(k),'type'),'text')==1
            
             set(axes_handles(k),'FontSize',fontSize(2))

        end

    end
    end
end